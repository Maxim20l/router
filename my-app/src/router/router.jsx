import { createBrowserRouter } from "react-router-dom";
import { Layout } from "../pages/Layout";
import { HomePage } from '../pages/HomePage';
import { BasketPage } from "../pages/BasketPage";
import { FavoritePage } from "../pages/IceCreamPage";
import ErrorPage from "../pages/ErrorPage";
import { getProducts } from "../components/helpers/getBasketProducts";
export const router = createBrowserRouter([
    {
        path: '/',
        element: <Layout/>,
        errorElement: <ErrorPage />,
        children: [
            {
                element: <HomePage />,
                index: true,
            },
            {
                path: '/basket',
                element: <BasketPage />,
                loader: getProducts,
            },
            {
                element: <FavoritePage />,
                path: '/favorite',
                loader: getProducts,
            }
        ]
    }
])