import styled from "styled-components";
import { Container } from "../../Container";
import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Card } from "./components/card";


const ContainerCards = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    gap: 8px;
`

function Cards({openModalToBuy}) {
    const [arrCards, setArrCards] = useState([]);
    
    useEffect(() => {
        const sendRequest = async (url) => {
            try {
                const response = await fetch(url);
                const result = await response.json();
                setArrCards(result.product);
                return result.product;
            } catch (error) {
                throw new Error("Помилка!");
            }
        }
        sendRequest('./iceCreamProducts.json');
    }, [])

    

    const card = arrCards.map(elem => {
        return (
            <Card key={elem.articul} data={elem} openModalToBuy={openModalToBuy} />
        )
    })

    return (
        <>
            <Container>
                <ContainerCards>
                    {card}
                </ContainerCards>
            </Container>
        </>
    )
}

export default Cards;