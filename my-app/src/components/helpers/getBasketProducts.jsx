 function getProducts() {
     const sendRequest = async (url) => {
            try {
                const response = await fetch(url);
                const result = await response.json();
                return result.product;
            } catch (error) {
                throw new Error("Помилка!");
            }
        }
        return sendRequest('./iceCreamProducts.json');
}

export { getProducts };