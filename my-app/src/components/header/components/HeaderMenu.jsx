import styled from 'styled-components';
import { IoIosBasket, IoIosIceCream } from "react-icons/io"
import { Container } from '../../../Container';
import { NavLink } from 'react-router-dom';
import { CounterBasket } from '../../counterBasket'
import { CounterFavorite } from'../../counterFavorite'
import { useState, useEffect } from 'react';


const ContactUsBtn = styled.a.attrs({
    href: '#',
})`
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 700;
    font-size: 19px;
    text-decoration: none;
    text-align: center;
    border: 1px solid black;
    border-radius: 8px;
    padding: 6px;
    color: white;
    background-color: #CC2A41;
    border-width: 0px;
    transition: 0.9s;

    &:hover {
        color: #CC2A41;
        background: white;
    }
`;

const HeaderEl = styled.header`
    position: fixed;
    width: 100%;
    background-color: #FEC77E;
    z-index: 2;
`;
const Wrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 6px;
`;



function HeaderMenu() {

    const [countFavorite, setCountFavorite] = useState(0);

    useEffect(() => {
        if (!localStorage.getItem('arrayFavorite')) {
             setCountFavorite(countFavorite);
        }
        if (localStorage.getItem('arrayFavorite') !== null) {
            setCountFavorite(JSON.parse(localStorage.getItem('arrayFavorite')).length);
            console.log(countFavorite);
        }
    })

    return (
        <>
            <HeaderEl>
                <Container>
                    <Wrapper>
                        <ContactUsBtn>Contact Us</ContactUsBtn>
                        <NavLink to='/' className='logoName'>The Magic Slab</NavLink>
                        <div className="iconsContainer">
                            <NavLink to='/basket'><IoIosBasket /></NavLink>

                            <CounterFavorite countFavorite={countFavorite} />
                            <CounterBasket />
                            <NavLink to='/favorite'><IoIosIceCream /></NavLink>
                        </div>
                    </Wrapper>
                </Container>
            </HeaderEl>


        </>
    )
}


export default HeaderMenu;