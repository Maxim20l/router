import styled from 'styled-components';
import { useEffect } from 'react';
import PropTypes from 'prop-types';
// import { CounterBasket } from '../CounterBasket';
// import { CounterFavorite } from '../counterFavorite';
import { HeaderMenu } from './components';

const HeaderEl = styled.header`
    position: fixed;
    width: 100%;
    background-color: #FEC77E;
    z-index: 2;
`;
const Wrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 6px;
`;





function Header() {

    return (
        <>
            <div className="bgImg">
                <HeaderMenu />
            </div>
        </>
    )
}

// Header.propTypes = {
//     countBasket: PropTypes.number,
//     countFavorite: PropTypes.number,
// }

export default Header;


