import styled from "styled-components";
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';


const CountFav = styled.div`
    position: absolute;
    bottom: 8px;
    left: 55px
`;


function CounterFavorite({countFavorite}) {
    // const [countFavorite, setCountFavorite] = useState(0);

    // useEffect(() => {
    //     if (!localStorage.getItem('arrayFavorite')) {
    //          setCountFavorite(countFavorite);
    //     }
    //     if (localStorage.getItem('arrayFavorite') !== null) {
    //         setCountFavorite(JSON.parse(localStorage.getItem('arrayFavorite')).length);
    //         console.log(countFavorite);
    //     }
    // })
//   const actualCount = () => {
//         if (localStorage.getItem('arrayFavorite') === null) {
//             return countFavorite;
//         }
//         return JSON.parse(localStorage.getItem('arrayFavorite')).length;
//     }

    return (
        <CountFav>
            {/* <p>{actualCount()}</p> */}
            <p>{countFavorite}</p>
        </CountFav>
    )

}



export default CounterFavorite;