import styled from "styled-components";
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react'

const CountBasket = styled.div`
    position: absolute;
    bottom: 8px;
    left: 20px
`;


function CounterBasket() {
    const [countBasket, setCountBasket] = useState(0);

    useEffect(() => {
        if (localStorage.getItem('arrayBasket') === null) {
             setCountBasket(countBasket);
        }
        if (localStorage.getItem('arrayBasket') !== null) {
            setCountBasket(JSON.parse(localStorage.getItem('arrayBasket')).length);
       }
    })
    return (
        <>
            <CountBasket>
                <p>{countBasket}</p>
            </CountBasket>
        </>
    )
}

export default CounterBasket;