import { IceCream } from "../components/iceCream";
import { useState, useEffect } from "react";
import styled from "styled-components";
import { Container } from "../Container";
import {
    useLoaderData,
} from "react-router-dom";



const ContainerCards = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    gap: 8px;
    padding-top: 50px
    
`

function FavoritePage() {
    const products = useLoaderData();
    const [arrProductsFavorite, setArrProductsFavorite] = useState([]);
    useEffect(() => {
        let arr = [];
        for (let elem of products) {
            if (localStorage.getItem('arrayFavorite') === null) {
                localStorage.setItem('arrayFavorite', JSON.stringify([]));
            }
            if (JSON.parse(localStorage.getItem('arrayFavorite')).includes(elem.articul)) {
                console.log('hi');

                arr.push(elem);
            }

        }
        setArrProductsFavorite(arr);
    }, [])


    const card = arrProductsFavorite.map(elem => {

        return (
            <IceCream key={elem.articul} data={elem} />
        )


    })

    return (
        <>
            <Container>
                <ContainerCards>
                    {card}
                </ContainerCards>
            </Container>
        </>
    )
}

export { FavoritePage }