import { useRouteError } from "react-router-dom";
import styled from "styled-components";

export default function ErrorPage() {
    const error = useRouteError();
    console.error(error);

    const Oops = styled.h1`
        font-size: 40px;
    `
    const Apology = styled.p`
        margin: 50px;
    `
    const Problem = styled.p`
        color: grey;
    `
    return (
        <div id="error-page">
            <Oops>Oops!</Oops>
            <Apology>Sorry, an unexpected error has occurred.</Apology>
            <Problem><i>{error.statusText || error.message}</i></Problem>
        </div>
    );
}