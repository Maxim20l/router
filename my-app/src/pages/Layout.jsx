import { Outlet } from "react-router-dom";
import { Footer } from "../components/footer";
import { HeaderMenu } from "../components/header/components";


function Layout() {
    return (
        <>
            <div>
                <HeaderMenu />
                <main>
                    <Outlet></Outlet>
                </main>
            </div>
            <Footer />
        </>
    )
}


export { Layout };