import { Basket } from "../components/basket";
import { useState, useEffect } from "react";
import styled from "styled-components";
import { Container } from "../Container";
import {
    useLoaderData,
} from "react-router-dom";


const ContainerCards = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    gap: 8px;
    padding-top: 50px
    
`

function BasketPage() {
    const products = useLoaderData();
    const [arrProductsToBuy, setArrProductsToBuy] = useState([]);
    useEffect(() => {
        let arr = [];
        for (let elem of products) {
            if (localStorage.getItem('arrayBasket') === null) {
                localStorage.setItem('arrayBasket', JSON.stringify([]));
            }
            if (JSON.parse(localStorage.getItem('arrayBasket')).includes(JSON.stringify(elem.articul))) {
                arr.push(elem);
            }

        }
        setArrProductsToBuy(arr);
    }, [])


    const card = arrProductsToBuy.map(elem => {

        return (
            <Basket key={elem.articul} data={elem} />
        )


    })

    return (
        <>
            <Container>
                <ContainerCards>
                    {card}
                </ContainerCards>
            </Container>

        </>
    )
}

export { BasketPage }