import { Cards } from "../components/card";
import { Header } from '../components/header';
import { useEffect, useState } from 'react';
import { ModalConfirmToBuy } from "../components/modalConfirmToBuy";

function HomePage() {
    const [isModalToBuy, setIsModalToBuy] = useState(false);
    const [productToBuy, setProductToBuy] = useState(0);

    const openModalToBuy = (event) => {
        setProductToBuy(event.target.parentElement.id);
        setIsModalToBuy(!isModalToBuy);
    }

    const closeModalToBuy = (event) => {
        event.stopPropagation();
        if (event.target.id === 'modal-wrapper') {
            setIsModalToBuy(!isModalToBuy);
        } else if (event.target.dataset.close === 'true') {
            setIsModalToBuy(!isModalToBuy);
        }
    }

    const putElementIntoBascket = () => {
        if (localStorage.getItem('arrayBasket') === null) {
          localStorage.setItem('arrayBasket', JSON.stringify([]));
        }
        const arrProductsToBuy = JSON.parse(localStorage.getItem('arrayBasket'));
        arrProductsToBuy.push(productToBuy);
        localStorage.setItem('arrayBasket', JSON.stringify(arrProductsToBuy));
        setIsModalToBuy(!isModalToBuy);
      }
      

    return (
        <>
            <Header />
            <Cards openModalToBuy={openModalToBuy} />
            {isModalToBuy && <ModalConfirmToBuy closeModalToBuy={closeModalToBuy} putElementIntoBascket={putElementIntoBascket} />}
        </>
    )
}

export { HomePage }